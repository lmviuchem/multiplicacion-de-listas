from generador import GeneradorLista

class multiplicadorListas(GeneradorLista):

    def multiplicarListas(self, a, b):
        producto = [x*y for x,y in zip(a,b)]
        return producto