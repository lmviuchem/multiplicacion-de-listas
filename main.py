from generador import GeneradorLista
from multiplicador import multiplicadorListas

objeto = GeneradorLista()
listaA = objeto.generarListaUsuario(5)
print("Lista por el usuario: ", listaA)

objeto2 = GeneradorLista()
listaB = objeto2.generarListaAleatoria(5, 100, 200)
print("Lista por aleatorios: ", listaB)


objeto3 = multiplicadorListas()
resultado = objeto3.multiplicarListas(listaA, listaB)
print("El producto de las listas es: ", resultado)
