import random

class GeneradorLista:

    def __init__(self):
        self.listUsuario = []
        self.listAleatorio = []
        pass

    def generarListaUsuario(self, n):
        for i in range(n):
            numUsu = int(input("Ingrese número: "))
            self.listUsuario.append(numUsu)
        return self.listUsuario

    def generarListaAleatoria(self, n, x, y):
        for i in range(n):
            numAle = random.randint(x, y)
            self.listAleatorio.append(numAle)
        return self.listAleatorio
